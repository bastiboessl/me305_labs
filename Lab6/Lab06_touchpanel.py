''' @file                   Lab06_touchpanel.py
    @brief                  A driver for reading positions from the touchpanel
    @details                Creates a class which can be used to create a touchpanel object to get the position of the ball on the touchpanel
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   November 18, 2021
    @page touchpanel_doc    Touchpanel Driver Documentation
                            On this page you can find a detailed documentation of the touchpanel driver, including a general
			    description of the setup, testing procedure and results and a benchmark which shows how long the driver
			    takes to read position values from the panel.
    
    @section sec_setup      Hardware Setup
                            For this assignment we used the fully assembled ball-balancing plattform, which has a touchpanel on top
                            and is connected to the Nucleo STM32 microcontroller. In this setup we have a resistive touchpanel, 
                            which means that you can determine the position by the changing resistance, depending on where you press 
			    on the panel. Changing resistance leads to changing voltages at the pins connected to the panel, which can
			    be read via an ADC.
                            
                            Here is picture of the hardware setup:
                            \image html HardwareSetup.png "Hardware Setup"
                              
    
    @section sec_testing    Testing Procedure and Results
                            We tested the function of the touchpanel by performing X- and Y-Scans and comparing the read coordinates with
                            the known position we touched. In the center of the panel you should read (0, 0). In the pictures below you can
                            see that the positive x-direction is to the front and the postive y-direction is to the right (from the viewpoint
                            of the picture). The z-scan determines wheter there is something in contact with the panel at the moment or not.
                            
                            \image html TouchCenter.png "Touch in the center of the panel (x: 0mm, y: 0mm)"
                            
                            \image html TouchFrontRight.png "Touch in the front right of the panel (x: 74mm, y: 36mm)"
                            
    
    @section sec_benchmark  Benchmark
                            We took a few measures to optimize the speed of our xyz-scan function. First thing we did is to define every
                            constant value as a micropython.const(). We did this for example with the center coordinates that have to be computed
                            in the calculation of the actual coordinate. We also precomputed the scale factor in x- and y-direction and defined
                            it as a constant to reduce the calculation time. 
                            With these measures we were able to achieve an average reading time of 1239us for all coodrinates (xyz) together
                            over a total of 100 scans. With this speed we can be sure, that the touchpanel reading is fast enough to not conflict 
                            with other parts of final project, like the controller driver.
                            
                            
    
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   November 18, 2021
'''

import pyb
import utime
from micropython import const
import time

class Touchpanel:
    ''' @brief          Interface with touchpanel
    '''
    
    def __init__(self, x_m, y_m, x_p, y_p, width , length, x_center, y_center):
        ''' @brief          Constructs an touchpanel object
            @param x_m negativ x-Pin 
            @param y_m negativ y-Pin
            @param x_p positiv x-Pin
            @param y_p positive y-Pin
            @param length  length of the touchpanel
            @param width widths of the touchpanel
            @param x_center coordinate of the center of the touchpanel
            @param y_center coordinate of the center of the touchpanel
        '''
        #class variables
        self.x_m = pyb.Pin(x_m)   
        self.y_m = pyb.Pin(y_m) 
        self.x_p = pyb.Pin(x_p)  
        self.y_p = pyb.Pin(y_p) 
        self.width = width
        self.length = length
        self.x_center = const(x_center)
        self.y_center = const(y_center)
        self.scale_x = const(self.width/4095)
        self.scale_y = const(self.length/4095)
        self.x_old = 0
        self.old_time = 0
        self.new_time = 0
        
    def x_scan(self):
        ''' @brief     returns ball x-position with respect to center
            @return x-position with respect to the center
        '''
        #x-scan
        self.x_m.init(mode = pyb.Pin.OUT_PP) #sets x_m to pushpull output
        self.x_m.value(0) #set output to low
        self.y_m.init(mode = pyb.Pin.IN) #sets y_m to an input to read the voltage
        self.y_p.init(mode = pyb.Pin.IN) #sets y_p to an input to float it 
        self.x_p.init(mode = pyb.Pin.OUT_PP) #sets x_p to pushpull output
        self.x_p.value(1) #set output to high
        

        ADC = pyb.ADC(self.y_m) #give y_m Pin to read the voltage    
        
        #self.x_new = ((ADC.read()*self.scale_x-self.x_center)-self.x_old)*0.85+self.x_old+
        
         
        return((ADC.read()*self.scale_x-self.x_center)) #formula to calculate x-position with respect to the center
    
    def y_scan(self):
        ''' @brief     returns ball y-position with respect to center
            @return y-position with respect to the center
        '''
        #x-scan
        self.x_m.init(mode = pyb.Pin.IN) #sets x_m to pushpull output
        self.y_m.init(mode = pyb.Pin.OUT_PP) #sets y_m to an input to read the voltage
        self.y_m.value(0) #set output to low
        self.y_p.init(mode = pyb.Pin.OUT_PP) #sets y_p to an input to float it 
        self.x_p.init(mode = pyb.Pin.IN) #sets x_p to pushpull output
        self.y_p.value(1) #set output to high

        ADC = pyb.ADC(self.x_m) #give y_m Pin to read the voltage
        
        
        
        return(ADC.read()*self.scale_y-self.y_center) #formula to calculate x-position with respect to the center
    
    def z_scan(self):
        ''' @brief     returns true or false value wether there is a contact with the touchpanel or not
            @return returns true or false value wether there is a contact with the touchpanel or not
        '''
        self.x_m.init(mode = pyb.Pin.OUT_PP) #sets x_m to pushpull output
        self.y_m.init(mode = pyb.Pin.IN) #sets y_m to an input to read the voltage
        self.x_m.value(0) #set output to low
        self.y_p.init(mode = pyb.Pin.OUT_PP) #sets y_p to an input to float it 
        self.x_p.init(mode = pyb.Pin.IN) #sets x_p to pushpull output
        self.y_p.value(1) #set output to high

        ADC = pyb.ADC(self.y_m) #give y_m Pin to read the voltage
        ADC_value = ADC.read()
        
        if(ADC_value > 4080):
            return(False)
        else:
            return(True)
        
    def all_scan(self):
        return(self.x_scan(),self.y_scan(), self.z_scan())


while(1):
    touchpanel = Touchpanel(pyb.Pin.cpu.A1, pyb.Pin.cpu.A0, pyb.Pin.cpu.A7, pyb.Pin.cpu.A6, 176, 100, 88, 50)
    while(1):
        i = 0
        start = utime.ticks_us()
        while(i <100):   
            touchpanel.all_scan()
            i +=1
        stop = utime.ticks_us()
        
        time.sleep(2)
        print('Time Difference: ', utime.ticks_diff(stop,start)/100)
        print('x: ', touchpanel.x_scan(), 'y: ', touchpanel.y_scan(), 'Touch: ', touchpanel.z_scan())


