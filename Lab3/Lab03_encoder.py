''' @file                   Lab03_encoder.py
    @brief                  A driver for reading from Quadrature Encoders
    @details                Creates a class which can be used to create a encoder object to get the position of a encoder, set the position of a encoder to 0 and to get the delta of a encoder..
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 7, 2021

'''

import pyb

class Encoder:
    ''' @brief          Interface with quadrature encoders
    '''
    
    def __init__(self, pin1, pin2, timer_number):
        ''' @brief          Constructs an encoder object
            @param pin1 value of the first pin of the encoder
            @param pin2 value of the second pin of the encoder
            @timer_number value of the timer number
        '''
        #class variables
        #adjusted value for the current encoder position
        self.position = 0  
        #Encoder Position of the previous run             
        self.oldPosition = 0  
        #Current Encoder Position          
        self.actualPosition = 0         
        #Delta between oldPosition and actualPosition
        self.delta = 0                  
        
        #creates a encoder pin object
        encoder_pin1 = pyb.Pin (pin1)       
        #sets settings of the timer                                        
        self.tim = pyb.Timer(timer_number, prescaler = 0, period = 65535)   
        #creates encoder timer Channel1        
        self.tim.channel (1, pyb.Timer.ENC_AB, pin = encoder_pin1)                   
        #creates a encoder 2 pin object
        encoder_pin2 = pyb.Pin (pin2)                
        #creates encoder timer Channel2                               
        self.tim.channel (2, pyb.Timer.ENC_AB, pin = encoder_pin2)                   
        

    def update(self):
        ''' @brief          Updates encoder position and delta
        '''
        #gets the Encoder Position of the prevoius run
        self.oldPosition = self.actualPosition    
        #updates the encoder position          
        self.actualPosition = self.tim.counter()  
        #calculates the delta           
        self.delta = self.get_delta()                       
        
        #calculations the adjusted value for the current encoder position
        if(self.delta < -32768):                            #corrects the overflow
            self.position += (self.delta + 65535)
        elif(self.delta > 32768):                           #corrects the underflow
            self.position += (self.delta - 65535)
        else:                                               #adds normal delta
            self.position += self.delta
    
    def get_position(self):
        ''' @brief          returns encoder postition
            @return         The new position of the encoder shaft
        '''
        #returns new position of encoder
        return self.position
    
    def set_position(self, position):
        ''' @brief          Sets encoder position
            @param position The new position of the encoder shaft
        '''
        #sets the new position
        self.position = position                           
        
        
    def get_delta(self):
        ''' @brief          Returns encoder delta
            @return         The change in position of the encoder shaft between the two most recent updates
        '''
        #calculates and returns the delta
        return(self.actualPosition - self.oldPosition)      
        
    

