''' @file                   Lab03_task_encoder.py
    @brief                  with that file you can access the encoder driver functions
    @details
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 7, 2021

'''

import encoder
import utime
import math

#Define State Variables
S0_Init = 0
S1_Update = 1


class Task_Encoder:
    '''@brief Constructs an Task_encode object which gets commands from user_input and interacts with the encoder driver to manipulate the positions or get data from the encoder.
    '''
    def __init__(self,period,pin1, pin2, timer_number, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta):
        ''' @brief Constructs an Task_encoder object
            @param period defines the next time the task is going to run
            @param pin1 contains the number of the pin 1 for the encoder
            @param pin2 contains the number of the pin 2 for the encoder
            @param timer_number contains the value of the timer number
            @param get_zero_pos queue which zeros the position of the encoder
            @param get_pos queue which gets the position of the encoder
            @param get_del queue which gets the delta of the encoder
            @param collect_data queue which is the command to collect data of the encoder for 30 sec
            @param data_list queue which contains a time and position of the encoder
            @param collect_time shared variable which contains the value of a time
            @param pos shared variable which contains the current position of the encoder
            @param delta shared variable which contains the current delta of the encoder
            @details
        '''
        #class variables
        #initializes the first state
        self.state = S0_Init                   
        #counts the number of iterations                         
        self.runs = 0               
        #defines the next time the task is going to run                                    
        self.period = period                           
        #defines the next time the task is going to run                 
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period) 
        
        #sets shared variables + queues as class variables
        self.get_zero_pos = get_zero_pos
        self.get_pos = get_pos
        self.get_del = get_del
        self.collect_data = collect_data
        self.data_list = data_list
        self.collect_time = collect_time
        self.pos = pos
        self.delta = delta
        self.pin1 = pin1 
        self.pin2 = pin2
        self.timer = timer_number
        
    def run(self):
        ''' @brief          runs one interation of the task
            @details
        '''
        #checks if it is time to run the task
        if (utime.ticks_us() >= self.next_time):                                    
            #checks the current state
            if (self.state == S0_Init):                                             
                #run state 0
                #creates encoder object
                self.encoder = encoder.Encoder(self.pin1, self.pin2, self.timer)  
                
                #transition to state 1
                self.state = S1_Update
                
            #checks the current state
            if (self.state == S1_Update):                                           
                #run state 1
                #updates the encoder position
                self.encoder.update()                                              
            
                #check shared variables for commands
                
                #zero position
                if (self.get_zero_pos.num_in() > 0):
                    #gets the item from the queue
                    self.get_zero_pos.get()        
                    #sets encoder position to 0                                 
                    self.encoder.set_position(0)                                      
                    
                #gets position
                if (self.get_pos.num_in() > 0):
                    #gets the item from the queue
                    self.get_pos.get()    
                    #sets the shared variable to the current position                                          
                    self.pos.write(self.encoder.get_position())                    
                                 
                #get delta
                if (self.get_del.num_in() > 0):
                    #gets the item from the queue
                    self.get_del.get()         
                    #sets the shared variable to the current delta                                     
                    self.delta.write(self.encoder.get_delta())                     
                             
                #collect data
                if (self.collect_data.num_in() > 0):
                    #gets the item from the queue
                    self.collect_data.get() 
                    #sends the current encoder position and time to the queue                                        
                    self.data_list.put((self.collect_time.read(), (self.encoder.get_position()), self.encoder.get_delta())) 
            #defines the next time the task is going to run 
            self.next_time = utime.ticks_add(self.next_time, self.period)           
            
            
            
                





