''' @file Lab03_motordriver.py
    @brief                  contains 2 classes to create a motor object and driver
    @details                contains a class called DRV8847 to create a motor driver and configure the DRV8847 which can be used to perform motor control. Also contains a class called motor which can be used to apply PWM to a motor
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 25, 2021
'''
import pyb
import utime

class DRV8847:
    ''' @brief A motor driver class for the DRV8847 from TI.
    @details Objects of this class can be used to configure the DRV8847
    motor driver and to create one or moreobjects of the
    Motor class which can be used to perform motor
    control.
    
    Refer to the DRV8847 datasheet here:
    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self, nSleep, nFault):
        ''' @brief Initializes and returns a DRV8847 object.
            @param nSleep parameter to initialize the Sleep Pin object 
            @param nFault paramtere to initialize the Fault Pin object
        '''
        self.sleepPin = pyb.Pin(nSleep, mode=pyb.Pin.OUT_PP)
        self.faultPin = pyb.Pin(nFault)
        self.faultInt = pyb.ExtInt (self.faultPin, mode = pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback = self.fault_cb)
        
    
    def enable (self):
        ''' @brief Brings the DRV8847 out of sleep mode.
        '''
        #disable fault interrupt
        self.faultInt.disable()                
        #Re-enable the motor driver
        self.sleepPin.high() 
        #Wait for the fault pin to return high                     
        utime.sleep_us(25)
        #Re-enable the fault interrupt
        self.faultInt.enable()                 
        
    
    def disable (self):
        ''' @brief Puts the DRV8847 in sleep mode.
        '''
        #disables the motor
        self.sleepPin.low()
        
    
    def fault_cb (self, IRQ_src):
        ''' @brief Callback function to run on fault condition.
        @param IRQ_src The source of the interrupt request.
        '''
        #disables the motors
        self.disable()
        print('---------------------')
        print('Fault detected')
        print('---------------------')
    
    def motor (self, pinIn1, pinIn2, chNum1, chNum2, TimerNum):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
        @param pinIn1 value of Pin1 of motor
        @param pinIn2 value of Pin2 of motor
        @param chNum1 value of Timer Channel 1
        @param chNum2 value of Timer Channel 2
        @param TimerNum value of Timer number
        @return An object of class Motor
        '''
        #creates motor object
        return Motor(pinIn1, pinIn2, chNum1, chNum2, TimerNum)

class Motor:
    ''' @brief A motor class for one channel of the DRV8847.
    @details Objects of this class can be used to apply PWM to a given
    DC motor.
    '''
    
    def __init__ (self, pinIn1, pinIn2, chNum1, chNum2, TimerNum):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
        @details Objects of this class should not be instantiated
        directly. Instead create a DRV8847 object and use
        that to create Motor objects using the method
        DRV8847.motor().
        @param pinIn1 value of Pin1 of motor
        @param pinIn2 value of Pin2 of motor
        @param chNum1 value of Timer Channel 1
        @param chNum2 value of Timer Channel 2
        @param TimerNum value of Timer number
        '''
        ##@brief creates timer object
        #
        self.timer = pyb.Timer(TimerNum, freq=20000)
        ##@brief initialize timer channel 1
        #
        self.tch1 = self.timer.channel(chNum1, pyb.Timer.PWM, pin=pinIn1)
        ##@brief initialize timer channel 2
        #
        self.tch2 = self.timer.channel(chNum2, pyb.Timer.PWM, pin=pinIn2)
        
    
    def set_duty (self, duty):
    
        ''' @brief Set the PWM duty cycle for the motor channel.
        @details This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed number holding the duty
        cycle of the PWM signal sent to the motor
        '''
        #checks if the duty cycle is negativ or positive
        if (duty > 0): #forward
            self.tch1.pulse_width_percent(duty)
            self.tch2.pulse_width_percent(0)
            
        elif (duty < 0): #backward
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent((-1)*duty)
            
        else: #zero
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(0)
        
