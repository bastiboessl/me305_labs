''' @file                   Lab03_task_motor.py
    @brief                  with that file you can access the motor driver functions
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 23, 2021
'''

import motordriver
import utime
import pyb

#Define State Variables
##@brief defines the initialization state
#
S0_Init = 0
##@brief defines the update state to interact with the motor driver functions
#
S1_Update = 1


class Task_Motor:
    ''' @brief A motor task class
    @details Objects of this class can be used to control the motor.
    It determines how often it will interact with the motor and sets the duty cycle or clears a fault of the motor.
    It communicates with the task_user object and gets the instructions from there
    '''

    def __init__(self, period, duty_m1, duty_m2, clear_fault):
        ''' @brief creates a object of Task_Motor
            @param period defines the time until task_motor will run again
            @param duty_m1 queue item which contains the value to set the duty cycle of the first motor
            @param duty_m2 queue item which contains the value to set the duty cycle of the second motor
            @param clear_fault que item to clear the faults of the motors
        '''
        
        #class variables
        self.state = S0_Init
        self.runs = 0
        self.period = period
        #defines when the task will run again 
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
        #shared variables
        self.duty_m1 = duty_m1
        self.duty_m2 = duty_m2
        self.clear_fault = clear_fault
        
    def run(self):
        ''' @brief          runs one interation of the task
        '''
        #checks if it is time to run the task
        if (utime.ticks_us() >= self.next_time):
            
            #initialization state
            if (self.state == S0_Init):
                #run state 0
                #creates motordriver object
                self.motor_drv = motordriver.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2)
                #creates motor object 1
                self.motor_1 = self.motor_drv.motor(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2, 3)
                #creatse motor object 2
                self.motor_2 = self.motor_drv.motor(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4, 3)
                #enables the motors
                self.motor_drv.enable()
                
                #transition to state 1
                self.state = S1_Update
                
            #update state
            if (self.state == S1_Update):
                #run state 1
                                
                #check shared variables for commands
                
                #set duty cycle motor 1
                if (self.duty_m1.num_in() > 0):
                    self.motor_1.set_duty(self.duty_m1.get())
                    
                
                #set duty cycle motor 2
                if (self.duty_m2.num_in() > 0):
                    duty = self.duty_m2.get()
                    self.motor_2.set_duty(duty)
                    
                #clear faults of the motors
                if (self.clear_fault.num_in() > 0):
                    self.clear_fault.get()
                    self.motor_drv.enable()
                    
            #defines the next time the run should run
            self.next_time = utime.ticks_add(self.next_time, self.period)