
''' @file                   Lab03_task_user.py
    @brief                  with that file you can access the user interface functions
    @details
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 7, 2021

'''


import utime
import pyb
import math
#Define State Variables
S0_Init = 0
S1_PrintUI = 1
S2_WaitForInput = 2
S3_ZeroPosition = 3
S4_GetPosition = 4
S5_GetDelta = 5
S6_CollectData = 6
S7_StopCollectData = 7
S8_PrintPosition = 8
S9_PrintDelta = 9
S10_ZeroPosition_2 = 10
S11_GetPosition_2 = 11
S12_GetDelta_2 = 12
S13_CollectData_2 = 13
S14_StopCollectData_2 = 14
S15_PrintPosition_2 = 15
S16_PrintDelta_2 = 16
S17_ChangeVelocity = 17
S18_ChangeVelocity_2 = 18
S19_ClearFault = 19

class Task_User:
    ''' @brief a class to create a User_Task
    @details a way to interact with the user. Prints out statements for the user and reads the user input. The information is shared with the motor and encoder_task
    '''
    def __init__(self,period, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta, duty_m1, duty_m2, clear_fault, get_zero_pos_2, get_pos_2, get_del_2, collect_data_2, data_list_2, pos_2, delta_2):
        ''' @brief          Constructs an Task_user object
            @param period defines the next time the task is going to run
            @param get_zero_pos queue which zeros the position of the encoder
            @param get_pos queue which gets the position of the encoder
            @param get_del queue which gets the delta of the encoder
            @param collect_data queue which is the command to collect data of the encoder for 30 sec
            @param data_list queue which contains a time and position of the encoder
            @param collect_time shared variable which contains the value of a time
            @param pos shared variable which contains the current position of the encoder
            @param delta shared variable which contains the current delta of the encoder
            @param duty_m1 queue which sets the duty cycle of the motor 1
            @param duty_m2 queue which sets the duty cycle of the motor 2
            @param clear_fault queue which clears the fault of the motor
            @param get_zero_pos_2 queue which zeros the position of the encoder 2
            @param get_pos_2 queue which gets the position of the encoder 2
            @param get_del_2 queue which gets the delta of the encoder 2
            @param collect_data_2 queue which is the command to collect data of the encoder for 30 sec
            @param data_list_2 queue which contains a time and position of the encoder
            @param pos_2 shared variable which contains the current position of the encoder
            @param delta_2 shared variable which contains the current delta of the encoder
        '''
        #class variables
        #defines current state
        self.state = S0_Init            
        #counts the number of iterations                                
        self.runs = 0          
        #defines the next time the task is going to run                                         
        self.period = period 
        #defines the next time the task is going to run                                          
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period) 
        
        #initalizes shared variables
        self.get_zero_pos = get_zero_pos
        self.get_pos = get_pos
        self.get_del = get_del
        self.collect_data = collect_data
        self.data_list = data_list
        self.collect_time = collect_time
        self.pos = pos
        self.delta = delta
        self.get_zero_pos_2 = get_zero_pos_2
        self.get_pos_2 = get_pos_2
        self.get_del_2 = get_del_2
        self.collect_data_2 = collect_data_2
        self.data_list_2 = data_list_2
        self.pos_2 = pos_2
        self.delta_2 = delta_2
        self.duty_value = ''
        self.duty_m1 = duty_m1
        self.duty_m2 = duty_m2
        self.clear_fault = clear_fault
        
    def run(self):
        ''' @brief          runs one interation of the task
        '''
        #checks if it is time to run the task
        if (utime.ticks_us() >= self.next_time):                        
            
            #checks the current state
            if (self.state == S0_Init):                                 
                #run state 0
                #creates a serport object
                self.serport = pyb.USB_VCP()                            
                #transition to state 1
                self.state = S1_PrintUI
                
            #checks the current state
            if (self.state == S1_PrintUI):                              
                #run state 1               
                #print User Interface
                print('-------------------------------------------------------------------------------------------')
                print("\n\nChoose one of the following commands:\n")
                print("'z'\tZero the position of encoder 1")
                print("'p'\tPrint out the position of encoder 1")
                print("'d'\tPrint out the delta for encoder 1")
                print("'g'\tCollect encoder 1 data for 30 seconds and print it to PuTTY as a comma separated list")
                print("'s'\tEnd data collection prematurely of encoder 1")
                print("'Z'\tZero the position of encoder 2")
                print("'P'\tPrint out the position of encoder 2")
                print("'D'\tPrint out the delta for encoder 2")
                print("'G'\tCollect encoder 2 data for 30 seconds and print it to PuTTY as a comma separated list")
                print("'m'\tEnter duty cycle for motor 1")
                print("'M'\tEnter duty cycle for moto 2")
                print("'c' or 'C'\tClear fault of the motors")
                print("'S'\tEnd data collection prematurely for encoder 2")
                print('-------------------------------------------------------------------------------------------')
                
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
        
            #checks if it is time to run the task
            if (self.state == S8_PrintPosition):                        
                #run state 8
                #prints the current Encoder position
                print('----------------------------------------------------')
                print('Current encoder 1 position is: ', self.pos.read()) 
                print('----------------------------------------------------')
                
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
                
            #checks if it is time to run the task
            if (self.state == S15_PrintPosition_2):                        
                #run state 8
                #prints the current Encoder position
                print('----------------------------------------------------')
                print('Current encoder 2 position is: ', self.pos_2.read()) 
                print('----------------------------------------------------')
                
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
                
            #checks if it is time to run the task
            if (self.state == S9_PrintDelta):                           
                #run state 9
                #prints the current delta
                print('----------------------------------------------------')
                print('Current encoder 1 delta is: ', self.delta.read())
                print('----------------------------------------------------')
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
                
            #checks if it is time to run the task
            if (self.state == S16_PrintDelta_2):                           
                #run state 9
                #prints the current delta
                print('----------------------------------------------------')
                print('Current encoder 2 delta is: ', self.delta_2.read())
                print('----------------------------------------------------')
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
        
            #checks if it is time to run the task
            if (self.state == S2_WaitForInput):                         
                #run state 2
                #checks if the user put in something and checks what the input was
                self.check_user_input()  
                                            
            #checks the current state
            if (self.state == S3_ZeroPosition):                 
                #send instruction in the queue to task_encoder to zero the position of the encoder
                self.get_zero_pos.put(1)
                print('----------------------------------------------------')
                print('Position of Encoder 1 set to zero!\n\n')
                print('----------------------------------------------------')
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
                
            #checks the current state
            if (self.state == S10_ZeroPosition_2):                
            #send instruction in the queue to task_encoder to zero the position of the encoder 2
                self.get_zero_pos_2.put(1)
                print('----------------------------------------------------')
                print('Position of Encoder 2 set to zero!\n\n')
                print('----------------------------------------------------')
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
                
            #checks the current state
            if (self.state == S4_GetPosition):                  
                #send instruction in the queue to task_encoder to get the position of the encoder
                self.get_pos.put(1)
                #transition to the next state
                self.state = S8_PrintPosition
            
            #checks the current state
            if (self.state == S11_GetPosition_2):                  
                #send instruction in the queue to task_encoder to get the position of the encoder
                self.get_pos_2.put(1)
                #transition to the next state
                self.state = S15_PrintPosition_2
                
            #checks the current state
            if (self.state == S5_GetDelta):                     
                #send instruction in the queue to task_encoder to get the delta of the encoder
                self.get_del.put(1)
                #transition to the next state
                self.state = S9_PrintDelta
                
            #checks the current state
            if (self.state == S12_GetDelta_2):                     
                #send instruction in the queue to task_encoder to get the delta of the encoder
                self.get_del_2.put(1)
                #transition to the next state
                self.state = S16_PrintDelta_2
                
            #checks the current state
            if (self.state == S6_CollectData):                  
                #start collecting data for 30s
                #checks if the current time is within the 30 sec
                if ((utime.ticks_diff(self.stop_time, utime.ticks_us())) > 0): 
                    #sends instruction to task_encoder to get the current encoder position
                    self.collect_data.put(1)     
                    #calculates the time how long the data collection has been running
                    self.collect_time.write((30000000- (utime.ticks_diff(self.stop_time, utime.ticks_us())))//1000) 
                    #stop collection before time ended by pressing s
                    #checks if the user entered something
                    if (self.serport.any()):        
                        #read input
                        self.user_in = self.serport.read(1)     
                        #checks if the user entered s
                        if (self.user_in.decode() == 's'):      
                        #transition to the next state    
                            self.state = S7_StopCollectData
                            self.user_in = ' '
                else:
                    #transition to the next state
                    self.state = S7_StopCollectData
                    
            if (self.state == S13_CollectData_2):                  #checks the current state
                #start collecting data for 30s
                if ((utime.ticks_diff(self.stop_time, utime.ticks_us())) > 0): #checks if the current time is within the 30 sec
                    self.collect_data_2.put(1)                    #sends instruction to task_encoder to get the current encoder position
                    self.collect_time.write((30000000- (utime.ticks_diff(self.stop_time, utime.ticks_us())))//1000) #calculates the time how long the data collection has been running
                    
                    #stop collection before time ended by pressing s
                    if (self.serport.any()):                    #checks if the user entered something
                        self.user_in = self.serport.read(1)     #read input
                        
                        if (self.user_in.decode() == 'S'):      #checks if the user entered s
                        #transition to the next state    
                            self.state = S14_StopCollectData_2
                            self.user_in = ' '
                            print('----------------------------------------------------')
                            print('Stop Collecting Data')
                            print('----------------------------------------------------')
                else:
                    #transition to the next state
                    self.state = S14_StopCollectData_2
                    print('----------------------------------------------------')
                    print('Stop Collecting Data')
                    print('----------------------------------------------------')
                
            #checks the current state
            if (self.state == S7_StopCollectData):              

                #print the data list with the collected encoder positions
                print('----------------------------------------------------')
                print('Collected data:')
                print('time[s], Pos[rad], velocity[rad/s]')
                while (self.data_list.num_in() > 0):
                    data = self.data_list.get()
                    print(data[0]/1000,data[1]/4000*2*math.pi, data[2]/4000*2*math.pi/0.002) 
                print('\n\n')
                print('End Data List')
                print('----------------------------------------------------')
                #transition to the next state
                self.state = S2_WaitForInput
                                    
            if (self.state == S14_StopCollectData_2):              #checks the current state

                #print the data list with the collected encoder positions
                print('Collected data:')
                print('time[s], Pos[rad], velocity[rad/s]')
                while (self.data_list_2.num_in() > 0):
                    data = self.data_list_2.get()
                    print(data[0]/1000,data[1]/4000*2*math.pi, data[2]/4000*2*math.pi/0.002)
                
                print('\n\n')
                print('End Data List')
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
                
            #checks the current state
            if (self.state == S17_ChangeVelocity):  
                #clears previous user_inputs
                user_duty_cycle = b''
                #sets counter to 1 (reset), counts the number of user inputs
                counter = 1
                #checks if the user input was a "Enter"
                while(user_duty_cycle.decode() != '\r'):
                    #checks if there is a user input
                    if (self.serport.any()):                  
                        #read input
                        user_duty_cycle = self.serport.read(1)  
                        #checks if the number of inputs is below 5 (maximum of 5 digits input is possible)
                        if ((counter >= 2) and (counter <= 5)):
                            #checks if the user input was a "Enter"
                            if(user_duty_cycle.decode() != '\r'):
                                #checks if the input was a digit number
                                if(user_duty_cycle.decode().isdigit()):
                                    #decodes the value into a string and adds it to the duty_value
                                    self.duty_value += user_duty_cycle.decode()
                                    #increases the number of inputs to 1
                                    counter += 1
                                else:
                                    #prints error message
                                    print("You entered no number!")
                                    print("Try again:\n")
                        #checks the first input of the user
                        if (counter == 1):
                            #checks if the input was a minus or a plus
                            if(user_duty_cycle.decode() == '-' or user_duty_cycle.decode() == '+' ):
                                #decodes the value into a string and adds it to the duty_value
                                self.duty_value += user_duty_cycle.decode()
                                #sets the counter to 2
                                counter = 2
                            else:
                                #prints error message 
                                print("You have to type in a minus or a plus")
                                print("Try again:\n")
                #if user did not enter something it sets duty cycle to 0
                if(self.duty_value == '' or self.duty_value == '+' or self.duty_value == '-'):
                    self.duty_value += '0'
                #checks if the user input was within -100 and 100
                if (int(self.duty_value) <= 100 and int(self.duty_value) >= -100):
                    #sets the duty cycle by sending a task to task_motor
                    self.duty_m1.put(int(self.duty_value))
                    print("Duty set was succesfull set to:", int(self.duty_value))
                else:
                    #prints error message
                    print("The number entered was either too high or too low. It has to be within the range -100 and +100")
                    
                self.duty_value = ''                         
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
                
            #checks the current state
            if (self.state == S18_ChangeVelocity_2):              
                   #clears previous user_inputs
                   user_duty_cycle = b''
                   #sets counter to 1 (reset), counts the number of user inputs
                   counter = 1
                   #checks if the user input was a "Enter"
                   while(user_duty_cycle.decode() != '\r'):
                       #checks if there is a user input
                       if (self.serport.any()):                  
                           #read input
                           user_duty_cycle = self.serport.read(1)  
                           #checks if the number of inputs is below 5 (maximum of 5 digits input is possible)
                           if ((counter >= 2) and (counter <= 5)):
                               #checks if the user input was a "Enter"
                               if(user_duty_cycle.decode() != '\r'):
                                   #checks if the input was a digit number
                                   if(user_duty_cycle.decode().isdigit()):
                                       #decodes the value into a string and adds it to the duty_value
                                       self.duty_value += user_duty_cycle.decode()
                                       #increases the number of inputs to 1
                                       counter += 1
                                   else:
                                       #prints error message
                                       print("You entered no number!")
                                       print("Try again:\n")
                           #checks the first input of the user
                           if (counter == 1):
                               #checks if the input was a minus or a plus
                               if(user_duty_cycle.decode() == '-' or user_duty_cycle.decode() == '+' ):
                                   #decodes the value into a string and adds it to the duty_value
                                   self.duty_value += user_duty_cycle.decode()
                                   #sets the counter to 2
                                   counter = 2
                               else:
                                   #prints error message 
                                   print("You have to type in a minus or a plus")
                                   print("Try again:\n")
                   #if user did not enter something it sets duty cycle to 0
                   if(self.duty_value == '' or self.duty_value == '+' or self.duty_value == '-'):
                       self.duty_value += '0'
                   #checks if the user input was within -100 and 100
                   if (int(self.duty_value) <= 100 and int(self.duty_value) >= -100):
                       #sets the duty cycle by sending a task to task_motor
                       self.duty_m2.put(int(self.duty_value))
                       print("Duty set was succesfull set to:", int(self.duty_value))
                   else:
                       #prints error message
                       print("The number entered was either too high or too low. It has to be within the range -100 and +100")
                       
                   self.duty_value = ''                         
                   #transition to the next state
                   self.state = S2_WaitForInput
                   print('----------------------------------------------------')
                   print('Wait for user input...')
                   print('----------------------------------------------------')
                
            #checks the current state
            if (self.state == S19_ClearFault): 
                #sets instruction to task_motor to clear the fault
                self.clear_fault.put(1)        
                #transition to the next state
                self.state = S2_WaitForInput
                print('----------------------------------------------------')
                print('Wait for user input...')
                print('----------------------------------------------------')
            #defines the next time the task is going to run  
            self.next_time = utime.ticks_add(self.next_time, self.period)       
            
    def check_user_input(self):
        ''' @brief          Checks if and what letter the user entered
        '''
        #checks if there is a user input
        if (self.serport.any()):                    
            #read input            
            self.user_in = self.serport.read(1)                 
            #checks if the user input is equal to z
            if (self.user_in.decode() == 'z'):                  
                #transition to state 3 - zero the position of the encoder  
                self.state = S3_ZeroPosition
                self.user_in = ' '
            #checks if the user input is equal to p     
            elif (self.user_in.decode() == 'p'):                
                #transition to state 4 - get the position of the encoder
                self.state = S4_GetPosition
                self.user_in = ' '
            #checks if the user input is equal to d
            elif (self.user_in.decode() == 'd'):                
                #transition to state 5 - get the delta of the encoder
                self.state = S5_GetDelta
                self.user_in = ' '
            #checks if the user input is equal to g                        
            elif (self.user_in.decode() == 'g'):       
                #transition to state 6 - collect encoder 1 data for 30 seconds and print it to PuTTY as a comma separated list
                self.state = S6_CollectData
                #defines when the data collection should stop
                self.stop_time = utime.ticks_add(utime.ticks_us(), 30000000)    
                self.user_in = ' '
            #checks if the user input is equal to Z
            elif (self.user_in.decode() == 'Z'):                
                #transition to state 10 - zero the position of the encoder 2
                self.state = S10_ZeroPosition_2
                self.user_in = ' '
            #checks if the user input is equal to P
            elif (self.user_in.decode() == 'P'):                
            #transition to state 11 - get the position of the encoder 2
                self.state = S11_GetPosition_2
                self.user_in = ' '
            #checks if the user input is equal to D                        
            elif (self.user_in.decode() == 'D'):   
                #transition to state 12 - get the delta of the encoder 2
                self.state = S12_GetDelta_2
                self.user_in = ' '
            #checks if the user input is equal to m                      
            elif (self.user_in.decode() == 'm'):                
                print('Type in a duty cycle between -100 and 100 in this form: for example ''+50'' or ''-50'' and press enter to activate:\n')
                #transition to state 17 - change the velocity of the motor
                self.state = S17_ChangeVelocity
            #checks if the user input is equal to M
            elif (self.user_in.decode() == 'M'):                
                print('Type in a duty cycle between -100 and 100 in this form: for example ''+50'' or ''-50'' and press enter to activate:\n')
                #transition to state 17 - change the velocity of the motor 2
                self.state = S18_ChangeVelocity_2   
            #checks if the user input is equal to G                      
            elif (self.user_in.decode() == 'G'):  
                #transition to state 13 - collect encoder 2 data for 30 seconds and print it to PuTTY as a comma separated list
                self.state = S13_CollectData_2
                #defines when the data collection should stop
                self.stop_time = utime.ticks_add(utime.ticks_us(), 30000000)    
                self.user_in = ' ' 
            #checks if the user input is equal to c or C
            elif ((self.user_in.decode() == 'C') or (self.user_in.decode() == 'c')):
                #transition to state 19 - clear the fault of the motors                
                self.state = S19_ClearFault
                self.user_in = ' '
            else:
                print('----------------------------------------------------')
                print('Unknown user input: Try again')
                print('----------------------------------------------------')