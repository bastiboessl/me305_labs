''' @file                   Lab03_main.py
    @brief                  In that file the main program is executed
    @details                There are different files associated with this lab:<br>
			    Click \ref Lab03_task_user.py "here" to get to the file which allows to read user input.<br>
			    Click \ref Lab03_task_encoder.py "here" to get to the file which communicates and controls the encoder.<br>
			    Click \ref Lab03_encoder.py "here" to get to the file which contains the driver for the encoder.<br>
		   	    Click \ref Lab03_shares.py "here" to get to the file which allows to create shares and queues.<br>
		   	    Click \ref Lab03_task_motor.py "here" to get to the file which allows to control the motor.<br>
		   	    Click \ref Lab03_motordriver.py "here" to get to the file which contains the driver for the motor.<br>
			    To access the source code click here: https://bitbucket.org/bastiboessl/me305_labs/src/master/Lab3/ <br>
			    These are the final state machines we used:
                            \image html lab03_Task_Encoder.png "Task Encoder" width=70% 
                            \image html lab03_Task_Motor.png "Task Motor" width=70% 
                            \image html lab03_Task_User.png "Task User" width=70%  
			    These are the Task Diagrams we used:
			    \image html lab03_Task_Diagram.png "Task Diagram" width=70% 
			
			    To show we are able to collect data from both encoders, here are two example plots:
			    \image html lab03_plotM1.jpg "Plot Encoder 1 Data" width=70% 
			    \image html lab03_plotM2.jpg "Plot Encoder 2 Data" width=70% 
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 26, 2021

'''

import task_encoder
import task_user
import task_motor
import shares
import pyb

if __name__ == '__main__':
    
    #shared variables
    ## @brief sends instruction task from task_user to task_encoder to set the encoder to zero
    #
    get_zero_pos = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position of encoder 1
    #
    get_pos = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the delta position from the encoder
    #
    get_del = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position and time
    #
    collect_data = shares.Queue()
    ## @brief sends current encoder position and time from task_encoder to task_user
    #
    data_list = shares.Queue()
    ## @brief holds the time how long the data collection has been running
    #
    collect_time = shares.Share()
    ## @brief holds the current encoder position
    #
    pos = shares.Share()
    ## @brief holds the current delta of encoder 1
    #
    delta = shares.Share()
    ## @brief holds the value of the duty cycle for motor 1
    #
    duty_m1 = shares.Queue()
    ## @brief holds the value of the duty cycle for motor 2
    #
    duty_m2 = shares.Queue()
    ## @brief sends instruction from task_user to task_motor to clear the faults of the motors
    #
    clear_fault = shares.Queue()
    ## @brief sends instruction task from task_user to task_encoder to set the encoder 2 to zero
    #
    get_zero_pos_2 = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position of encoder 2
    #
    get_pos_2 = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the delta position from the encoder 2
    #
    get_del_2 = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position and time of encoder 2
    #
    collect_data_2 = shares.Queue()
    ## @brief sends current encoder position and time from task_encoder to task_user
    #
    data_list_2 = shares.Queue()
    ## @brief holds the current encoder 2 position
    #
    pos_2 = shares.Share()
    ## @brief holds the current delta of encoder 2
    #
    delta_2 = shares.Share()
    
    
    #initiating tasks
    encoder1 = task_encoder.Task_Encoder(2000, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta)
    encoder2 = task_encoder.Task_Encoder(2000, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, get_zero_pos_2, get_pos_2, get_del_2, collect_data_2, data_list_2, collect_time, pos_2, delta_2)
    user = task_user.Task_User(40000, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta, duty_m1, duty_m2, clear_fault,get_zero_pos_2, get_pos_2, get_del_2, collect_data_2, data_list_2, pos_2, delta_2)
    motor = task_motor.Task_Motor(2000, duty_m1, duty_m2, clear_fault)
    
    while(True):
        
        #try to run the different tasks
        try:
            encoder1.run()
            encoder2.run()
            motor.run()
            user.run()
        #checks if there is a KeyboardInterrupt
        except KeyboardInterrupt:
            break
    #Prints the errror message
    print('Program terminating')
