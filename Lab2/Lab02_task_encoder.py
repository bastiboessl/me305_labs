''' @file                   Lab02_task_encoder.py
    @brief                  with that file you can access the encoder driver functions
    @details
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 7, 2021

'''

import encoder
import utime
import pyb

#Define State Variables
S0_Init = 0
S1_Update = 1


class Task_Encoder:
    def __init__(self,period, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta):
        ''' @brief          Constructs an Task_encoder object
            @details
        '''
        #class variables
        self.state = S0_Init                                            #initializes the first state
        self.runs = 0                                                   #counts the number of iterations
        self.period = period                                            #defines the next time the task is going to run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period) #defines the next time the task is going to run
        
        #sets shared variables + queues as class variables
        self.get_zero_pos = get_zero_pos
        self.get_pos = get_pos
        self.get_del = get_del
        self.collect_data = collect_data
        self.data_list = data_list
        self.collect_time = collect_time
        self.pos = pos
        self.delta = delta
        
    def run(self):
        ''' @brief          runs one interation of the task
            @details
        '''
        if (utime.ticks_us() >= self.next_time):                                    #checks if it is time to run the task
            
            if (self.state == S0_Init):                                             #checks the current state
                #run state 0
                self.encoder1 = encoder.Encoder(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 3)  #creates encoder object
                
                #transition to state 1
                self.state = S1_Update
                
            if (self.state == S1_Update):                                           #checks the current state
                #run state 1
                self.encoder1.update()                                              #updates the encoder position
                
                
                #check shared variables for commands
                
                #zero position
                if (self.get_zero_pos.num_in() > 0):
                    self.get_zero_pos.get()                                         #gets the item from the queue
                    self.encoder1.set_position(0)                                   #sets encoder position to 0
                    
                    
                #gets position
                if (self.get_pos.num_in() > 0):
                    self.get_pos.get()                                              #gets the item from the queue
                    self.pos.write(self.encoder1.get_position())                    #sets the shared variable to the current position
                         
                    
                #get delta
                if (self.get_del.num_in() > 0):
                    self.get_del.get()                                              #gets the item from the queue
                    self.delta.write(self.encoder1.get_delta())                     #sets the shared variable to the current delta
                             
                
                #collect data
                if (self.collect_data.num_in() > 0):
                    self.collect_data.get()                                         #gets the item from the queue
                    self.data_list.put((self.collect_time.read(), self.encoder1.get_position())) #sends the current encoder position and time to the queue
                
            self.next_time = utime.ticks_add(self.next_time, self.period)           #defines the next time the task is going to run
            
            
            
                





