
''' @file                   Lab02_task_user.py
    @brief                  with that file you can access the user interface functions
    @details
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 7, 2021

'''


import utime
import pyb

#Define State Variables
S0_Init = 0
S1_PrintUI = 1
S2_WaitForInput = 2
S3_ZeroPosition = 3
S4_GetPosition = 4
S5_GetDelta = 5
S6_CollectData = 6
S7_StopCollectData = 7
S8_PrintPosition = 8
S9_PrintDelta = 9

class Task_User:
    def __init__(self,period, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta):
        ''' @brief          Constructs an Task_user object
            @details
        '''
        #class variables
        self.state = S0_Init                                            #defines current state
        self.runs = 0                                                   #counts the number of iterations
        self.period = period                                            #defines the next time the task is going to run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period) #defines the next time the task is going to run
        
        #initalizes shared variables
        self.get_zero_pos = get_zero_pos
        self.get_pos = get_pos
        self.get_del = get_del
        self.collect_data = collect_data
        self.data_list = data_list
        self.collect_time = collect_time
        self.pos = pos
        self.delta = delta
        
    def run(self):
        ''' @brief          runs one interation of the task
            @details
        '''
        
        if (utime.ticks_us() >= self.next_time):                        #checks if it is time to run the task
            
            if (self.state == S0_Init):                                 #checsk the state
                #run state 0
                self.serport = pyb.USB_VCP()                            #creates a serport object
                #transition to state 1
                self.state = S1_PrintUI
                
            if (self.state == S1_PrintUI):                              #checks the current state
                #run state 1               
                #print User Interface
                print("\n\nChoose one of the following commands:\n")
                print("'z'\tZero the position of encoder 1")
                print("'p'\tPrint out the position of encoder 1")
                print("'d'\tPrint out the delta for encoder 1")
                print("'g'\tCollect encoder 1 data for 30 seconds and print it to PuTTY as a comma separated list")
                print("'s'\tEnd data collection prematurely\n\n")
                
                #transition to the next state
                self.state = S2_WaitForInput
        
            if (self.state == S8_PrintPosition):                        #checks if it is time to run the task
                #run state 8
                #prints the current Encoder position
                print('Current encoder position is: ', self.pos.read(), '\n\n') 
                #transition to the next state
                self.state = S2_WaitForInput
                
            if (self.state == S9_PrintDelta):                           #checks if it is time to run the task
                #run state 9
                #prints the current delta
                print('Current encoder delta is: ', self.delta.read(), '\n\n')
                #transition to the next state
                self.state = S2_WaitForInput
        
            if (self.state == S2_WaitForInput):                         #checks if it is time to run the task
                #run state 2
                if (self.serport.any()):                                #checks if there is a user input
                    self.user_in = self.serport.read(1)                 #read input
                    
                    if (self.user_in.decode() == 'z'):                  #checks if the user input is equal to z
                        #transition to state 3 - zero the position of the encoder  
                        self.state = S3_ZeroPosition
                        self.user_in = ' '
                    
                    elif (self.user_in.decode() == 'p'):                #checks if the user input is equal to p     
                        #transition to state 4 - get the position of the encoder
                        self.state = S4_GetPosition
                        self.user_in = ' '
                    
                    elif (self.user_in.decode() == 'd'):                #checks if the user input is equal to d
                        #transition to state 5 - get the delta of the encoder
                        self.state = S5_GetDelta
                        self.user_in = ' '
                        
                    elif (self.user_in.decode() == 'g'):                #checks if the user input is equal to g                        #transition to state 6 - collect encoder 1 data for 30 seconds and print it to PuTTY as a comma separated list
                        self.state = S6_CollectData
                        self.stop_time = utime.ticks_add(utime.ticks_us(), 30000000)    #defines when the data collection should stop
                        self.user_in = ' '
                                            
                        
            if (self.state == S3_ZeroPosition):                 #checks the current state
                #send instruction in the queue to task_encoder to zero the position of the encoder
                self.get_zero_pos.put(1)
                print('Position set to zero!\n\n')
                #transition to the next state
                self.state = S2_WaitForInput
                
            if (self.state == S4_GetPosition):                  #checks the current state
                #send instruction in the queue to task_encoder to get the position of the encoder
                self.get_pos.put(1)
                #transition to the next state
                self.state = S8_PrintPosition
                
            if (self.state == S5_GetDelta):                     #checks the current state
                #send instruction in the queue to task_encoder to get the delta of the encoder
                self.get_del.put(1)
                #transition to the next state
                self.state = S9_PrintDelta
                
            if (self.state == S6_CollectData):                  #checks the current state
                #start collecting data for 30s
                if ((utime.ticks_diff(self.stop_time, utime.ticks_us())) > 0): #checks if the current time is within the 30 sec
                    self.collect_data.put(1)                    #sends instruction to task_encoder to get the current encoder position
                    self.collect_time.write((30000000- (utime.ticks_diff(self.stop_time, utime.ticks_us())))//1000) #calculates the time how long the data collection has been running
                    
                    #stop collection before time ended by pressing s
                    if (self.serport.any()):                    #checks if the user entered something
                        self.user_in = self.serport.read(1)     #read input
                        
                        if (self.user_in.decode() == 's'):      #checks if the user entered s
                        #transition to the next state    
                            self.state = S7_StopCollectData
                            self.user_in = ' '
                else:
                    #transition to the next state
                    self.state = S7_StopCollectData
                
            if (self.state == S7_StopCollectData):              #checks if the current state

                #print the data list with the collected encoder positions
                print('Collected data:')
                while (self.data_list.num_in() > 0):
                    print(self.data_list.get())
                
                print('\n\n')
                #transition to the next state
                self.state = S2_WaitForInput
                                    
            self.next_time = utime.ticks_add(self.next_time, self.period)       #defines the next time the task is going to run
