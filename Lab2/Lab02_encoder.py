''' @file                   Lab02_encoder.py
    @brief                  A driver for reading from Quadrature Encoders
    @details
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 7, 2021

'''

import pyb

class Encoder:
    ''' @brief          Interface with quadrature encoders
        @details
    '''
    
    def __init__(self, pin1, pin2, timer_number):
        ''' @brief          Constructs an encoder object
            @details
        '''
        #class variables
        self.position = 0               #adjusted value for the current encoder position
        self.oldPosition = 0            #Encoder Position of the previous run
        self.actualPosition = 0         #Current Encoder Position
        self.delta = 0                  #Delta between oldPosition and actualPosition
        
        encoder_pin1 = pyb.Pin (pin1)                                               #creates pin object for Encoder1
        self.tim = pyb.Timer(timer_number, prescaler = 0, period = 65535)           #sets settings of the timer
        self.tim.channel (1, pyb.Timer.ENC_A, pin = encoder_pin1)                   #creates encoder timer Channel1

        encoder_pin2 = pyb.Pin (pin2)                                               #creates pin object for Encoder1
        self.tim.channel (2, pyb.Timer.ENC_B, pin = encoder_pin2)                   #creates encoder timer Channel2
        

    def update(self):
        ''' @brief          Updates encoder position and delta
            @details
        '''
        
        self.oldPosition = self.actualPosition              #gets the Encoder Position of the prevoius run
        self.actualPosition = self.tim.counter()            #updates the encoder position
        self.delta = self.get_delta()                       #calculates the delta 
        
        #calculations the adjusted value for the current encoder position
        if(self.delta < -32768):                            #corrects the overflow
            self.position += (self.delta + 65535)
        elif(self.delta > 32768):                           #corrects the underflow
            self.position += (self.delta - 65535)
        else:                                               #adds normal delta
            self.position += self.delta
    
    def get_position(self):
        ''' @brief          returns encoder postition
            @details
            @return         The new position of the encoder shaft
        '''
        
        return self.position
    
    def set_position(self, position):
        ''' @brief          Sets encoder position
            @details
            @param position The new position of the encoder shaft
        '''
        
        self.position = position                            #sets the new position
        
        
    def get_delta(self):
        ''' @brief          Returns encoder delta
            @details
            @return         The change in position of the encoder shaft between the two most recent updates
        '''
        return(self.actualPosition - self.oldPosition)      #calculates and returns the delta
        
    

