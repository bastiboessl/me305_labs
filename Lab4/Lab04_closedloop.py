''' @file                   Lab04_closedloop.py
    @brief                  A controller driver for closed loop control
    @details                Creates a class which can be used to set up a closed loop control system
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   October 28, 2021

'''


class ClosedLoop:
    ''' @brief          
    '''
    
    def __init__(self, gain, limit):
        ''' @brief          Constructs an closed loop controller object
            @param gain value of the proportional gain
            @param limit saturation limit of the PWM level
        '''
        
        #class variables
        self.Kp = gain
        self.limit = limit

    def run(self, Q_set, Q_act):
        #calculate the PWM for the motor
        output = (Q_set-Q_act)*(self.Kp)
        #check if output is over 100%
        if (output >= 100):
            output = 100
        elif (output <= -100):
            output = -100
        else:
            pass
        #return output PWM for the motor
        return (int(output))
    
    def get_Kp(self):

        return self.Kp                   
     
         
    def set_Kp(self, gain):
        
        self.Kp = gain
        
    

