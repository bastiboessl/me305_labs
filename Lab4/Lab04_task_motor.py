''' @file                   Lab04_task_motor.py
    @brief                  with that file you can access the motor driver functions
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   November 3, 2021
'''

import motordriver
import utime
import pyb
import closedloop
import math

#Define State Variables
##@brief defines the initialization state
#
S0_Init = 0
##@brief defines the update state to interact with the motor driver functions
#
S1_Update = 1


class Task_Motor:
    ''' @brief A motor task class
    @details Objects of this class can be used to control the motor.
    It determines how often it will interact with the motor and sets the duty cycle or clears a fault of the motor.
    It communicates with the task_user object and gets the instructions from there
    It also calls the controller driver to implement proportional controll on the motor
    '''

    def __init__(self, period, duty_m1, duty_m2, clear_fault, prop_gain, velocity, collect_actuationlevel, stepresp_actuationlevel_data, delta, fault, prop_gain_2, velocity_2, collect_actuationlevel_2, stepresp_actuationlevel_data_2, delta_2):
        ''' @brief creates a object of Task_Motor
            @param period defines the time until task_motor will run again
            @param duty_m1 queue item which contains the value to set the duty cycle of the first motor
            @param duty_m2 queue item which contains the value to set the duty cycle of the second motor
            @param clear_fault que item to clear the faults of the motors
            @param prop_gain queue that holds the current prop gain for the first motor
            @param prop_gain_2 queue that holds the current prop gain for the second motor
            @param velocity queue that holds the current set velocity for the first motor
            @param velocity_2 queue that holds the current set velocity for the second motor
            @param collect_actuationlevel sends instruction from task_user to task_motor to get the current actuationlevel for the first motor
            @param stepresp_actuationlevel_data sends current actuationlevel from task_motor to task_user for the first motor
            @delta delta shared variable which contains the current delta of the encoder 1
            @param collect_actuationlevel_2 sends instruction from task_user to task_motor to get the current actuationlevel for the second motor
            @param stepresp_actuationlevel_data_2 sends current actuationlevel from task_motor to task_user for the second motor
            @param delta_2 shared variable which contains the current delta of the encoder 2
            @param fault indicates a fault
        '''
        
        #class variables
        self.state = S0_Init
        self.runs = 0
        self.period = period
        #defines when the task will run again 
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.velocity_setpoint = 0
        self.closedloop_out = 0
        self.velocity_setpoint_2 = 0
        self.closedloop_out_2 = 0
        
        #shared variables
        self.duty_m1 = duty_m1
        self.duty_m2 = duty_m2
        self.clear_fault = clear_fault
        self.prop_gain = prop_gain
        self.velocity = velocity
        self.collect_actuationlevel = collect_actuationlevel
        self.stepresp_actuationlevel_data = stepresp_actuationlevel_data
        self.prop_gain_2 = prop_gain_2
        self.velocity_2 = velocity_2
        self.collect_actuationlevel_2 = collect_actuationlevel_2
        self.stepresp_actuationlevel_data_2 = stepresp_actuationlevel_data_2
        self.delta = delta
        self.delta_2 = delta_2
        self.fault = fault
        
    def run(self):
        ''' @brief          runs one interation of the task
        '''
        #checks if it is time to run the task
        if (utime.ticks_us() >= self.next_time):
            
            #initialization state
            if (self.state == S0_Init):
                #run state 0
                #creates motordriver object
                self.motor_drv = motordriver.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2, self.fault)
                #creates motor object 1
                self.motor_1 = self.motor_drv.motor(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2, 3)
                #creatse motor object 2
                self.motor_2 = self.motor_drv.motor(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4, 3)
                #enables the motors
                self.motor_drv.enable()
                
                #create a object of closedloop
                self.closedloop = closedloop.ClosedLoop(1,0)
                
                #transition to state 1
                self.state = S1_Update
                
            #update state
            if (self.state == S1_Update):
                #run state 1
                
                                
                #check shared variables for commands
                
                
                #set duty cycle motor 1
                if (self.duty_m1.num_in() > 0):
                    self.motor_1.set_duty(self.duty_m1.get())
                    
                
                #set duty cycle motor 2
                if (self.duty_m2.num_in() > 0):
                    duty = self.duty_m2.get()
                    self.motor_2.set_duty(duty)
                    
                
                #clear faults of the motors
                if (self.clear_fault.num_in() > 0):
                    self.clear_fault.get()
                    self.motor_drv.enable()
                    
                #sets the prop gain
                if (self.prop_gain.num_in() > 0):
                    #set prop gain
                    self.closedloop.set_Kp(self.prop_gain.get())
                    
                #sets the prop gain
                if (self.prop_gain_2.num_in() > 0):
                    #set prop gain
                    self.closedloop.set_Kp(self.prop_gain_2.get())
                    
                #sets a new velocity setpoint
                if (self.velocity.num_in() > 0):
                    self.velocity_setpoint = self.velocity.get()
                    
                #sets a new velocity setpoint
                if (self.velocity_2.num_in() > 0):
                    self.velocity_setpoint_2 = self.velocity_2.get()
                    
                #runs motor1 at velocity setpoint with closedloop control
                if (self.velocity_setpoint != 0):
                    #runs closedloop controller
                    self.closedloop_out = self.closedloop.run(self.velocity_setpoint, (self.delta.read()/4000*2*math.pi/0.005))
                    #print(self.velocity_setpoint, self.delta.read()/4000*2*math.pi/0.002)
                    #sets duty cycle to closedloop output
                    #muss vermutlich noch angepasst werden/ umgerechnet zu PWM
                    self.motor_2.set_duty(self.closedloop_out)
                    
                #runs motor1 at velocity setpoint with closedloop control
                if (self.velocity_setpoint_2 != 0):
                    #runs closedloop controller
                    self.closedloop_out_2 = self.closedloop.run(self.velocity_setpoint_2, (self.delta_2.read()/4000*2*math.pi/0.005))
                    #print(self.velocity_setpoint, self.delta.read()/4000*2*math.pi/0.002)
                    #sets duty cycle to closedloop output
                    #muss vermutlich noch angepasst werden/ umgerechnet zu PWM
                    self.motor_1.set_duty(self.closedloop_out_2)
                    
                #collect data for step response
                if (self.collect_actuationlevel.num_in() > 0):
                    #gets the item from the queue
                    self.collect_actuationlevel.get() 
                    #sends the current pwm level to the queue
                    #muss eventuell auch noch auf umgerechneten Wert geändert werden                                        
                    self.stepresp_actuationlevel_data.put(self.closedloop_out) 
                    
                #collect data for step response
                if (self.collect_actuationlevel_2.num_in() > 0):
                    #gets the item from the queue
                    self.collect_actuationlevel_2.get() 
                    #sends the current pwm level to the queue
                    self.stepresp_actuationlevel_data_2.put((self.closedloop_out_2))
                
            #defines the next time the run should run
            self.next_time = utime.ticks_add(self.next_time, self.period)