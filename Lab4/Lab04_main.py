''' @file                   Lab04_main.py
    @brief                  In that file the main program is executed
    @details                There are different files associated with this lab:<br>
			    Click \ref Lab04_task_user.py "here" to get to the file which allows to read user input.<br>
			    Click \ref Lab04_task_encoder.py "here" to get to the file which communicates and controls the encoder.<br>
			    Click \ref Lab04_encoder.py "here" to get to the file which contains the driver for the encoder.<br>
		   	    Click \ref Lab04_shares.py "here" to get to the file which allows to create shares and queues.<br>
		   	    Click \ref Lab04_task_motor.py "here" to get to the file which allows to control the motor.<br>
		   	    Click \ref Lab04_motordriver.py "here" to get to the file which contains the driver for the motor.<br>
			    Click \ref Lab04_closedloop.py "here" to get to the file which contains the closedloop control driver for the motor.<br>		
			    To access the source code click here: https://bitbucket.org/bastiboessl/me305_labs/src/master/Lab4/ <br>
			    More information about the program, the closed loop controller and our tuning results is available here: 
                            \ref lab04_Report "Lab Report and Diagrams of Lab04"
    @author                 Sebastian Bößl, Johannes Frisch
    @date                   November 3, 2021

'''

import task_encoder
import task_user
import task_motor
import shares
import pyb

if __name__ == '__main__':
    
    #shared variables
    ## @brief sends instruction task from task_user to task_encoder to set the encoder to zero
    #
    get_zero_pos = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position of encoder 1
    #
    get_pos = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the delta position from the encoder
    #
    get_del = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position and time
    #
    collect_data = shares.Queue()
    ## @brief sends current encoder position and time from task_encoder to task_user
    #
    data_list = shares.Queue()
    ## @brief holds the time how long the data collection has been running
    #
    collect_time = shares.Share()
    ## @brief holds the current encoder position
    #
    pos = shares.Share()
    ## @brief holds the current delta of encoder 1
    #
    delta = shares.Share()
    ## @brief holds the value of the duty cycle for motor 1
    #
    duty_m1 = shares.Queue()
    ## @brief holds the value of the duty cycle for motor 2
    #
    duty_m2 = shares.Queue()
    ## @brief sends instruction from task_user to task_motor to clear the faults of the motors
    #
    clear_fault = shares.Queue()
    ## @brief sends instruction task from task_user to task_encoder to set the encoder 2 to zero
    #
    get_zero_pos_2 = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position of encoder 2
    #
    get_pos_2 = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the delta position from the encoder 2
    #
    get_del_2 = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current position and time of encoder 2
    #
    collect_data_2 = shares.Queue()
    ## @brief sends current encoder position and time from task_encoder to task_user
    #
    data_list_2 = shares.Queue()
    ## @brief holds the current encoder 2 position
    #
    pos_2 = shares.Share()
    ## @brief holds the current delta of encoder 2
    #
    delta_2 = shares.Share()
    ## @brief holds the current prop_gain for the first motor
    #
    prop_gain = shares.Queue()
    ## @brief holds the current prop_gain for the second motor
    #
    prop_gain_2 = shares.Queue()
    ## @brief holds the current set velocity for the closedloop control for the first motor
    #
    velocity = shares.Queue()
    ## @brief holds the current set velocity for the closedloop control for the second motor
    #
    velocity_2 = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current velocity and time for the first encoder
    #
    collect_speed = shares.Queue()
    ## @brief sends instruction from task_user to task_encoder to get the current velocity and time for the second encoder
    #
    collect_speed_2 = shares.Queue()
    ## @brief sends instruction from task_user to task_motor to get the current actuationlevel for the first motor
    #
    collect_actuationlevel = shares.Queue()
    ## @brief sends instruction from task_user to task_motor to get the current actuationlevel for the second motor
    #
    collect_actuationlevel_2 = shares.Queue()
    ## @brief sends current encoder velocity and time from task_encoder to task_user for the first encoder
    #
    stepresp_encoder_data = shares.Queue()
    ## @brief sends current encoder velocity and time from task_encoder to task_user for the second encoder
    #
    stepresp_encoder_data_2 = shares.Queue()
    ## @brief sends current actuationlevel from task_motor to task_user for the first motor
    #
    stepresp_actuationlevel_data = shares.Queue()
    ## @brief sends current actuationlevel from task_motor to task_user for the second motor
    #
    stepresp_actuationlevel_data_2 = shares.Queue()
    ## @brief indicates a fault on the motor
    #
    fault = shares.Share()
    
    
    
    #initiating tasks
    encoder1 = task_encoder.Task_Encoder(5000, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta, collect_speed, stepresp_encoder_data)
    encoder2 = task_encoder.Task_Encoder(5000, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, get_zero_pos_2, get_pos_2, get_del_2, collect_data_2, data_list_2, collect_time, pos_2, delta_2, collect_speed_2, stepresp_encoder_data_2)
    user = task_user.Task_User(40000, get_zero_pos, get_pos, get_del, collect_data, data_list, collect_time, pos, delta, duty_m1, duty_m2, clear_fault,get_zero_pos_2, get_pos_2, get_del_2, collect_data_2, data_list_2, pos_2, delta_2, prop_gain, velocity, collect_speed, collect_actuationlevel, stepresp_encoder_data, stepresp_actuationlevel_data, fault, prop_gain_2, velocity_2, collect_speed_2, collect_actuationlevel_2, stepresp_encoder_data_2, stepresp_actuationlevel_data_2)
    motor = task_motor.Task_Motor(5000, duty_m1, duty_m2, clear_fault, prop_gain, velocity, collect_actuationlevel, stepresp_actuationlevel_data, delta, fault,  prop_gain_2, velocity_2, collect_actuationlevel_2, stepresp_actuationlevel_data_2, delta_2)
    
    while(True):
        
        #try to run the different tasks
        try:
            encoder1.run()
            encoder2.run()
            motor.run()
            user.run()
        #checks if there is a KeyboardInterrupt
        except KeyboardInterrupt:
            break
    #Prints the errror message
    print('Program terminating')
