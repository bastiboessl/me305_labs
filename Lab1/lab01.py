'''@file                    Lab01.py
   @brief                   Finite State Machine Skeleton Code
   @details                 This file will be used to demonstrate one method for implementing a finite state machine in Python
			    It is possible to cycle through three different LED patterns on the nucleo with this code. 			    The user has to press the blue button b1 on the nucleo to change the LED pattern.			    The LED pattern are the following:
		            a) Square Wave 
			    \image html square.png "Square"               		    b) Sine Wave 
			    \image html sine.png "Sine"                            c) Sawtooth Wave
			    \image html saw.png "Sawtooth"	
			    This is the finale state machine we used:

			    \image html lab01_fsm.jpg "State Machine used"			    
   @author                  Sebastian Boessl, Johannes Frisch
   @date                    October 6, 2021
'''

import utime
import pyb
import math


# Functions are defined above the main program code


def onButtonPressed(IRQ_src):
    '''@brief activates the buttonpressed variable       @param buttonpressed indicates whether the button was pressed        
    '''
    global btnpressed
    btnpressed = 1 
    
def reset_timer():
    """@brief this function resets the timer        @param startTime is a timer which saves the time when the function was started        
    """
    return utime.ticks_ms()
    
def update_timer(startTime):
    """ @brief this function stops the timer and calculates the difference between startTime and Stoptime        @param duration is the difference between startTime and stopTime            """
    stopTime = utime.ticks_ms()
    duration = utime.ticks_diff(stopTime, startTime)
    return duration
    
def update_sqwv(time):
    """ @brief this function creates the sawtooth pattern	@return return the brightness value for the LED    
    """
    return (100*((time%1000) < 500))

def update_stwv(time):
    ''' @brief this function creates the square pattern 	@return return the brightness value for the LED    
    '''
    return ((time%1000)/10)

def update_siwv(time):
    """ @brief this function creates the sine wave pattern	@return return the brightness value for the LED    
    """
    return (100*(0.5+(0.5*(math.sin((time/5000)*math.pi)))))



if __name__ == '__main__':
    ## @brief the next state to run as the FSM iterates
    #
    state = 0
    
    ## @brief The number of iterations of the FSM
    #
    runs = 0
    
    ## @brief indicates whether the button was pressed or not
    #
    btnpressed = 0
    startTime = reset_timer()
    duration = update_timer(startTime)
    
    while(True):
        
        try:
            if (state == 0):
                #run state 0 and initialize
                
                pinA5 = pyb.Pin (pyb.Pin.cpu.A5)                    #create pin object for LED
                tim2 = pyb.Timer(2, freq = 20000)                   #setup timer
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)   #setup PWM
                t2ch1.pulse_width_percent(0)                        #turns off the LED
                
                pinC13 = pyb.Pin (pyb.Pin.cpu.C13)                  #create pin object for button
                ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressed) #associate the callback function defined above with the button pin by setting up an external interrupt

                state = 1               #transition to state 1
                
                #print the welcome message and the instructions
                print('Welcome to the LED program!\nCycle through the different LED patterns by pressing the blue button on the nucleon!') 
            
            elif (state == 1):
                #run state 1
                
                if (btnpressed == 1):
                    btnpressed = 0
                    startTime = reset_timer()
                    state = 2               #transition to state 2
                    print('Square pattern selected')
                    
                    
            elif (state == 2):
                #run state 2
                
                #updates timer and updates the brightness of the LED
                duration = update_timer(startTime)
                t2ch1.pulse_width_percent(update_sqwv(duration))
                    
                if (btnpressed == 1):
                    btnpressed = 0
                    startTime = reset_timer()
                    state = 3               #transition to state 3
                    print('Sine pattern selected')
  
            elif (state == 3):
                #run state 3
                
                #updates timer and updates the brightness of the LED
                duration = update_timer(startTime)
                t2ch1.pulse_width_percent(update_siwv(duration))
                
                if (btnpressed == 1):
                    btnpressed = 0
                    startTime = reset_timer()
                    state = 4               #transition to state 4
                    print('Sawtooth pattern selected')
            
            elif (state == 4):
                #run state 4
                
                #updates timer and updates the brightness of the LED
                duration = update_timer(startTime)
                t2ch1.pulse_width_percent(update_stwv(duration))
                
                if (btnpressed == 1):
                    btnpressed = 0
                    startTime = reset_timer()
                    state = 2               #transition to state 2
                    print('Square pattern selected')
                
            runs += 1                   #increment run counter
        
        #makes sure that the program does not fail if you make a KeyboardInterrupt  
        except KeyboardInterrupt:
            break
        
    print('Program terminating')