'''@file                    Lab00.py
   @brief                   Calculates the fibonacci number
   @details                 This program asks for a integer user input and then calculates the desired fibonacci number
			    If the input is not a integer, a error message appears and asks for the user input again until the input is a positive integer
			    After the calculation the user can decide to continue or exit the program
'''



"Calculation of the Fibonacci number"
def fib(idx):
    '''
    @brief This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired Fibonacci number
    '''
    a = 0 #first fibonacci number
    b = 1 #second fibonacci number
    
    for i in range(idx): #for loop for the calculation of the fibonacci number until the desired index is reached
        a,b = b, a+b #adds the last two numbers
    fib = a #save the result of the calculation
    return(fib) #return the result
   
"Main function"
def my_function(): 
    """
    @brief This function asks for the user input, checks if the input is a integer and prints the result of the calculation of the desired fibonacci number
    """
    while(1): #starts the loop and exits if the user input is 'q'
        my_string = input('Please enter a index: ') #asks the user to put in the index for the fibonacci series
        while(not my_string.isdigit()): #checks if the input is a integer and loops until the input is a positive integer
            if (my_string[0] == '-'): #checks if the input could be a negative integer
                if my_string.lstrip("-").isdigit(): #removes the minus and then checks if the rest of the input is a integer
                    print('Input must be a positive integer') #prints the error message
                else:
                    print('Input must be a integer') #prints the error message
            else:
                print('Input must be a integer') #prints the error message
            my_string = input('Please enter a index: ') #asks again for user input
        idx = int(my_string) #converts the string into a integer
        print ('Fibonacci number at ''index {:} is {:}.'.format(idx,fib(idx))) #prints out the requested fibonacci number
        my_string = input('Type q if you want to quit, press enter if you want to continue: ') #asks if the user wants to continue
        if (my_string == 'q'): #checks exit condition
            break; #leaves the loop
            
"Only runs if module is not imported"
if __name__ == '__main__':
    my_function()
    

